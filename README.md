# Adthena technical test - Search classification

## How to use
### Training
Make sure you have a training dataset available, and ideally a GPU with CUDA installed (or a lot of time, since it will run on GPU if not)
The format expected for the training dataset is the same CSV format as the one given in the assignment.
```
py main.py train <path_to_trainset>
```
The program will output the trained model state dict (it will need to be used with an instance of the BERT classifier model)
as well as the statistics for each epoch of the learning in a separate file.

### Evaluation
You will need the state dict file for the model to us, as well as the file with searches to evaluate (same format as the one 
given for the assignemnt).
```
py main.py eval <path_to_model_statedict> <path_to_searches>
```
The program will output a CSV with 2 columns : search and prediction.

If you have the train and test set available in the same folder as the main, you can run the script as such:
```
>> py main.py train trainSet.csv
>> py main.py eval model_state_dict.pt candidateTestSet.txt
```

## Model chosen
I used a BERT classifier (which is a BERT model with an added linear layer for classification).
I chose it because BERT is one of the most used NLP algorithm and generally gives good results on a large range of problems.
It also gave me a chance to experience with Pytorch and deep learning. Although these are fields that
I really do not master yet, I thought it would be interesting to try and see if I can use them without problem
seeing as they are core in Adthena's stack.
It also comes pretrained with an english vocabulary so I did not have to worry about that part.
In hindsight, it might not have been the best choice for a short technical test because it takes 5 hours to train, even on GPU.

Speaking of GPU, installing everything to make it work on GPU and understanding how tensorflow and pytorch work was a bit
hard, but I am quite happy I got everything to work in the end.

## Pre-processing
There was not much pre processing required. To my delight the dataset seem to have been cleaned partially beforehand.
- Case was already lower
- Punctuation did not exist or wasn't there on most searches
- Most searches were really short
- There were numbers, but it was worth keeping them I think, as they were important for the meaning of the search
- I did not remove stop words since they are useful to keep with BERT.
- Similarly, it is not recommended to stem or lemmatize with BERT.

The only preprocessing I did was the encoding, which is all packaged in the BertTokenizer, in the encode plus function.
- Add [CLS] (for 'classification') and [SEP] (for end of sentence) tokens, which are required by BERT
- Map each token to a unique id usable by BERT
- Truncate or pad (with [PAD] tokens) to make all searches the same length
- Create the attention mask (which tells the model which tokens are 'relevant' and which are padding)

I also took advantage of the Pytorch dataloader to load data iteratively and avoid loading everything in memory.

## Methods used for evaluation
I separated my train and validation set in 85/15, and used loss and accuracy to evaluate the model.

I could have kept a 3rd set for testing afterward (which has not been used to evaluate the algorithm while training) but
decided not to, because it was supposed to be a short iteration, and I would not have much time to retrain the algorithm
multiple time afterwards anyway. Hopefully the results will be similar to what I saw on the validation test (and a quick
visual sanity check on the labels I gave on the test set available showed that the results seem coherent).

When it comes to my choice of metrics I chose to focus mainly on the accuracy for 4 reasons:
- The dataset is huge, so I am assuming the distribution of classes in my validation set is somewhat similar to the one
in the original training dataset.
- The classes are not fully balanced but there is no class that is particularly bigger than the rest and a lot of them contain
a lot of sample so the accuracy already gives me a good idea of the performance.
- I could have used an F-measure, but I had no benchmark to compare it to, so it would only have served to compare multiple 
runs with different parameters
- As I mentioned before, I wanted to save time, which is why I did not check the distribution of classes in the test set
(something like the train_test_split stratify argument in sklearn would have been good to implement but long to process on
such a large data set). Similarly, calculating f-measure or running the algorithm multiple times with different parameters was not
an option.

All of the previous point could be leads for future development on that algorithm, especially 
if we modify the hyperparameters such as the learning rate.

## Results

Overall I was pretty surprised with the results. Again, I do not have a proper benchmark to compare to and this
is a crude first shot at the problem so they could be improved I assume, but I was impressed at such a good accuracy
considering the large number of classes.

BERT is recommended to be run on 2 to 4 epochs and it seems that 4 was a good guess since
the accuracy seems to stabilize on the last run and the validation loss becomes bigger than
the training loss. Maybe the 4th epoch could be omitted, but any more epoch would probably lead
to overfitting.

```
  'Training Loss': 4.493480127108104,
  'Valid. Accur.': 0.4805084240392703,
  'Valid. Loss': 2.875635390070476,
  'epoch': 1

  'Training Loss': 2.468231076855075,
  'Valid. Accur.': 0.5596509233801614,
  'Valid. Loss': 2.213863581583992,
  'epoch': 2

  'Training Loss': 1.9543658945549611,
  'Valid. Accur.': 0.5883017588791383,
  'Valid. Loss': 2.017383603536765,
  'epoch': 3

  'Training Loss': 1.7219468857757843,
  'Valid. Accur.': 0.5974716112387481,
  'Valid. Loss': 1.9631297450218985,
  'epoch': 4
```
